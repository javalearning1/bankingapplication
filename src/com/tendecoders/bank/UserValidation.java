package com.tendecoders.bank;

import java.util.HashMap;
import java.util.Map;

public abstract class UserValidation implements BankAccount{


    Map<String,String> accountMap = new HashMap();

    private String userName;

    public UserValidation(){
        accountMap.put("John","johnpassword");
        accountMap.put("Muthu","muthupassword");
        accountMap.put("Mani","manipassword");
        accountMap.put("Nangai","nangaipassword");
        accountMap.put("Kumar","kumarpassword");

    }

    public void validate(String userName, String password) throws Exception {

        String passFromMap = accountMap.get(userName);
        //check if input is null
        if (passFromMap != null && passFromMap.equals(password)){
                System.out.println("Account login successful");

            }else {
            throw new Exception("username or password incorrect");
        }
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
}
